# Expense Reimbursement System
A little overview: In this simple website, I have made three users that are either Finance Managers or Employees. This is a simple reimbursement submission and approval site. 

## Starting the Application
By simply running the application in the Main Driver, you will see that the app is running on port 9012. You can change this if you wish or instead go to the [Home](http://localhost:9012/home) page. This will bring you to a login screen. 

## Signing in as an Employee
Using the credentials of username: ismith and password: bypass
You can log in to employee Ian Smith. Upon logging in you will be brought to the view requests page. Here you will see all the employee's past and pending requests. Now let's add another.

## Submitting a Request
Looking at the top left of the page you will see the Action Options drop down. Here there is a section for submit requests. Going to this a display will appear for submitting all the required information for a request. You must enter an amount, and say what the reimbursement is for. After submitting, you can go then view your new request by going back to your drop down and see your new request.

## Signing in as a Finance Manager
Before singing in as a finance manager we first have to log out! Its simple look in the header and select Log Out.
You will then be brought to the login page where you can use the credentials' username:bsmith password: password
to log into Bob Smith's account. Going to the View By drop down you can select All to see all reimbursement requests.

## Approving or Denying a Request
Now let's only view the pending requests. Go to the same drop down and select Pending. Here you will see all the pending requests. In the table under Approve or Deny select a drop down and change its status. If you go back to the pending page, you will see that one less request is pending. 
